/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

import java.util.Set;
import jp.co.bughouse.simpleblockchain.exception.UserNotFoundException;

/**
 *
 * @author まつのさん
 */
public class Main {
    public static void main(String[] args){
        try{
            User user1 = new User("テストユーザ1");
            User user2 = new User("テストユーザ2");
            User user3 = new User("テストユーザ3");

            Network.addUser(user1);
            Network.addUser(user2);
            Network.addUser(user3);

            Network.addTransaction(user1.sendMessage(Network.getUser(user2.getName()), "user1 -> user2"));
            Network.addTransaction(user2.sendMessage(Network.getUser(user3.getName()), "user2 -> user3"));
            Network.addTransaction(user3.sendMessage(Network.getUser(user2.getName()), "user3 -> user2"));


            createBlock(user1);
            createBlock(user2);

            Network.addTransaction(user1.sendMessage(Network.getUser(user3.getName()), "user1 -> user3"));
            createBlock(user3);

            createBlock(user2);


            for(Block block : Network.getBlock()){
                System.out.println(block);
            }
        }catch(UserNotFoundException e){
            System.err.println("ユーザおらへんで？" + e);
        }

    }
    
    
    public static void createBlock(User user){
        
        Set<SignedTransaction> userSignedTransaction = user.signed(Network.getTransactionSet());
        // サインしたトランザクションがない場合は何もしない
        if(userSignedTransaction == null){
            // あえてreturnを書くことで、
            // サイントランザクションがない場合は何もしないんだなぁという事を強調させる素敵な一文
            return;
        }else{
            // ブロック作成
            Block block = user.mining(userSignedTransaction, Network.getLastBlock());
            // ブロック追加
            Network.addBlock(block);
            // トランザクションからサイン済みトランザクションを削除
            Network.delTransaction(userSignedTransaction);
        }
    }
}
