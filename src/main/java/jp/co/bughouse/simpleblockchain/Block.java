/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * ブロックチェーンのブロックを表したクラス
 * @author まつのさん
 */
public class Block {
    private final Set<SignedTransaction> signedTrxSet;
    private final String nonce;
    private final Block prevBlock;
    private final Date timestamp;
    
    /**
     * こんこんこんすとらくた
     * @param signedTrxSet サイン済みトランザクションセット
     * @param nonce ハッシュ値を変えるための文字列（何でもいい）
     * @param prevBlock ひとつ前のブロック ジェネシスブロックの場合はnullが入る
     */
    public Block(Set<SignedTransaction> signedTrxSet, String nonce, Block prevBlock){
        this.signedTrxSet   = signedTrxSet;
        this.nonce          = nonce;
        this.prevBlock      = prevBlock;
        this.timestamp      = new Date();
    }
    
    /**
     * 出力したときに出る文字列 デバッグ用かな。
     * @return 文字列
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Block ");
        sb.append(new SimpleDateFormat("yyyyMMdd hh:mm:ss:SSS").format(timestamp)).append("\n");
        for(SignedTransaction signedTrx : signedTrxSet){
            sb.append(signedTrx).append("\n");
        }
        sb.append(nonce).append(" : ");
        sb.append(this.hashCode()).append(" : ");
        sb.append(prevBlock == null ? "null" : prevBlock.hashCode());
        
        return sb.toString();
    }
}
