/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author まつのさん
 */
public class Transaction {
    private final User fromUser;
    private final User toUser;
    private final String message;
    private final Date timestamp;
    
    /**
     * メッセージを送信したときのトランザクション
     * @param fromUser 送信元ユーザ
     * @param toUser 送信先ユーザ
     * @param message 送信されたメッセージ
     */
    // TODO: feeとかも実装しないといけないなーと思いつつ。。。
    public Transaction(User fromUser, User toUser, String message){
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.message = message;
        this.timestamp = new Date();
    }
    
    /**
     * メッセージ送信元ユーザを返す
     * @return メッセージ送信元ユーザ
     */
    public User getFromUser(){
        return this.fromUser;
    }
    
    /**
     * メッセージ送信先ユーザを返す
     * @return メッセージ送信先ユーザ
     */
    public User getToUser(){
        return this.toUser;
    }
    
    /**
     * メッセージを返す
     * @return メッセージ
     */
    public String getMessage(){
        return this.message;
    }
    
    /**
     * デバッグ用文字列 フォーマットはこんな感じ
     * yyyyMMdd hh:mm:ss:SSS : 送信元ユーザ名 : 送信先ユーザ名 : メッセージ :
     * @return 
     */
    @Override
    public String toString(){
        return new SimpleDateFormat("yyyyMMdd hh:mm:ss:SSS").format(timestamp) + ":" + fromUser.getName() + ":" + toUser.getName() + ":" + message + ":";
    }
}
