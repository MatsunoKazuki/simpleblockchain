/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang.RandomStringUtils;

/**
 * ブロックチェーンにおけるマイナーとウォレットをひとつにしたクラス
 * @author まつのさん
 */
public class User {
    private final String name;
    /**
     * こんこんこんすとらくた
     * @param name ユーザ名
     */
    public User(String name){
        this.name = name;
    }
    
    /**
     * ユーザ名を取得する
     * @return ユーザ名
     */
    public String getName(){
        return name;
    }
    
    /**
     * メッセージを送信し、トランザクションを作成する
     * @param toUser メッセージ送信先ユーザ
     * @param message メッセージ
     * @return メッセージ送信元、送信先、メッセージが入ったトランザクション
     */
    public Transaction sendMessage(User toUser, String message){
        // ユーザを取得する ユーザが居なければUserNotFoundExceptionを出力する
        return new Transaction(this, toUser, message);
    }
    
    /**
     * 新しいブロックを生成するメソッド
     * @param signedTrxSet サイン済みトランザクションのセット
     * @param prevBlock ひとつ前のブロック 
     *      作成するブロックがジェネシスブロックの場合はnullが入る
     * @return サイン済みトランザクションのセットを入れたブロック
     */
    public Block mining(Set<SignedTransaction> signedTrxSet, Block prevBlock){
        while(true){
            Block block = new Block(signedTrxSet, RandomStringUtils.randomAlphabetic(64), prevBlock);
            // ハッシュ値が決められた値以下の場合、ブロックを作成する
            if(checkBlockHash(block)){
                return block;
            }
        }
    }
    

    /**
     * トランザクションセットから、
     * 自分が関与していないトランザクションを取り出してサインする
     * @param trxSet トランザクションのセット
     * @return サイン済みトランザクションのセット
     */
    // 認証する
    public Set<SignedTransaction> signed(Set<Transaction> trxSet){
        // 自身が関連しないトランザクションのみを取り出す
        Set<Transaction> notMineTrx = getNotMineTransaction(trxSet);
        // サイン済みトランザクションのセット
        Set<SignedTransaction> signedTrxSet = new HashSet<>();
        for(Transaction trx : notMineTrx){
            // トランザクションにサイン(署名)する
            signedTrxSet.add(new SignedTransaction(trx, this));
        }
        
        // サインしたトランザクションセットを返す
        return signedTrxSet.isEmpty() ? null : signedTrxSet;
    }
    
    /**
     * ブロックのハッシュ値を見て、想定の値以下の場合trueを返す
     * ※今回は10桁中前7桁が0の場合(999以下の数)trueを返す
     * @param block チェックするブロック
     * @return 想定の値以下か、そうでないかをbooleanで返す
     */
    private boolean checkBlockHash(Block block){
        return String.format("%010d", block.hashCode()).startsWith("0000000");
    }
    
    /**
     * 自身の関与していないトランザクションのみのセットを返す
     * @param trxSet トランザクションセット
     * @return 関与していないトランザクションセット
     */
    private Set<Transaction> getNotMineTransaction(Set<Transaction> trxSet){
        Set<Transaction> notMineTrx = new HashSet<>();
        
        for(Transaction trx : trxSet){
            if(trx.getFromUser() != this && trx.getToUser() != this){
                notMineTrx.add(trx);
            }
        }
        
        return notMineTrx;
    }
    
}
