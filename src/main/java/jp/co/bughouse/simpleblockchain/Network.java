/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import jp.co.bughouse.simpleblockchain.exception.UserNotFoundException;

/**
 * ネットワークを表したクラス
 * @author まつのさん
 */
public class Network {
    
    private static Map<String, User> userMap;
    private static Set<Transaction> trxSet;
    private static Set<Block> blockSet;
    
    /**
     * あんまりなじみのないすたてぃっくこんすとらくたを使ってみた
     * 
     * userMap  = ネットワークに参加しているユーザのMap
     * trxSet   = ネットワークに参加しているトランザクションのSet
     * blockSet = ネットワークに参加しているブロックのSet
     */
    static{
        userMap     = new HashMap<>();
        trxSet      = new LinkedHashSet<>();
        blockSet    = new LinkedHashSet<>();
    }
    
    /**
     * ユーザをネットワークに追加する
     * @param user ユーザ
     */
    public static void addUser(User user){
        userMap.put(user.getName(), user);
    }
    
    /**
     * トランザクションをネットワークに追加する
     * @param trx トランザクション
     */
    public static void addTransaction(Transaction trx){
        System.out.println("add transaction : " + trx);
        trxSet.add(trx);
    }
    
    /**
     * ブロックをネットワークに追加する
     * @param block ブロック
     */
    public static void addBlock(Block block){
        blockSet.add(block);
    }
    
    /**
     * サイン済みトランザクションを、ネットワークに追加されているトランザクションから削除する
     * @param signedTrxSet サイン済みトランザクションのセット
     */
    public static void delTransaction(Set<SignedTransaction> signedTrxSet){
        for(SignedTransaction signedTrx : signedTrxSet){
            System.out.println("delete transaction : " + signedTrx);
            trxSet.remove(signedTrx.getTransaction());
        }
    }
    
    /**
     * ネットワークに登録されているユーザを取得する
     * @param name 取得するユーザ名
     * @return 取得するユーザ名に紐づくユーザ
     * @throws UserNotFoundException ユーザがいないときにはExceptionを返す
     */
    // TODO: 現状面倒くさかったので同一ユーザ名は入れられないようにしてある。
    public static User getUser(String name) throws UserNotFoundException{
        User user = userMap.get(name);
        if(user == null){
            throw new UserNotFoundException(name);
        }
        
        return user;
    }
    
    /**
     * ネットワークに登録されているトランザクションのセットを返す
     * @return トランザクションのセット
     */
    public static Set<Transaction> getTransactionSet(){
        return trxSet;
    }
    
    /**
     * ネットワークに登録されているブロックのうち
     * 最後に登録されたブロックを返す
     * 何も登録されていなければnullを返す
     * @return ブロック
     */
    public static Block getLastBlock(){
        // Blockの最後に追加されたオブジェクトを返す
        if(blockSet.isEmpty()){
            return null;
        }else{
            return blockSet.toArray(new Block[blockSet.size()])[blockSet.size() - 1];
        }
    }
    
    /**
     * ネットワークに登録されているブロックのセットを返す
     * デバッグ用かな
     * @return ブロックのセット
     */
    public static Set<Block> getBlock(){
        return blockSet;
    }
}
