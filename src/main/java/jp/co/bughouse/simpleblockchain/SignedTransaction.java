/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.bughouse.simpleblockchain;

/**
 *
 * @author まつのさん
 */
public class SignedTransaction {
    private final Transaction trx;
    private final User signedUser;
    /**
     * こんこんこんすとらくた
     * @param trx 未サインのトランザクション
     * @param signedUser サイン(署名)するユーザ
     */
    public SignedTransaction(Transaction trx, User signedUser){
        this.trx = trx;
        this.signedUser = signedUser;
    }
    
    /**
     * トランザクションを返す
     * @return トランザクション
     */
    public Transaction getTransaction(){
        return this.trx;
    }
    
    /**
     * デバッグ用文字列かな
     * サインしたユーザ名 : トランザクション詳細
     * が出るようになってる
     * @return 
     */
    @Override
    public String toString(){
        return signedUser.getName() + ":" + trx;
    }
}
